#pragma once
#include "MandelbrotSet.h"
#include "MandelbrotSetHalf.h"
#include "mbnamespace.h"

class MbSets
{
private:
	MandelbrotSet<float>* mbsetf;
	MandelbrotSet<double>* mbsetd;
	MandelbrotSet<long double>* mbsetld;
public:
	MbSets()
	{
		mbsetf = new MandelbrotSet<float>(100, 100);
		mbsetd = new MandelbrotSet<double>(100, 100);
		mbsetld = new MandelbrotSet<long double>(100, 100);
	}

	~MbSets()
	{
		delete mbsetf;
		delete mbsetd;
		delete mbsetld;
	}
	void* getByIndex(Mb::DataType type)
	{
		switch (type)
		{
			case Mb::Float:
				return mbsetf;
				break;
			case Mb::Double:
				return mbsetd;
				break;
			case Mb::Long_Double:
				return mbsetld;
				break;
		}
		return mbsetf;
	}
};
