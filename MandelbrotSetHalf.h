#pragma once

#include <complex>
#include <iostream>
#include <QImage>
#include <QPainter>
#include "mbnamespace.h"
#include <QLabel>
#include <QtConcurrent/QtConcurrent>

template <typename T>
class MandelbrotSetHalf
{
private:
	QImage* image = nullptr;
	Mb::ColorMode color_mode = Mb::ColorList;
	unsigned int width = 0, height = 0;
	std::complex<T> center = {0, 0};
	T radius = 2.0;
	int max_iterations = 50;
	float exponent = 2.0f;
	float max_magnitude = 4.0f;
	std::vector<T> hCoordinates, wCoordinates; // imaginary parts, real parts
	int* arrMaxIterations = nullptr;
	int colorWeights[3] = {8, 4, 4}; // R, G, B
	bool debug = true;

	int getMaxIterationAt(int wpos, int hpos)
	{
		if(hpos * width + wpos >= width*height)
		{
			std::cout << "Get out of Range: " << wpos << "," << hpos << std::endl;
			return -2;
			exit(EXIT_FAILURE);
		}
		return arrMaxIterations[hpos * width + wpos];
	}

	void setMaxIterationAt(int wpos, int hpos, int value)
	{
		if(hpos * width + wpos >= width*height)
		{
			std::cout << "Set out of Range: " << wpos << "," << hpos << std::endl;
			exit(EXIT_FAILURE);
		}
		arrMaxIterations[hpos * width + wpos] = value;
	}

	void clearArrMaxIterationAt()
	{
		for(int i = 0; i < width * height; i++)
		{
			arrMaxIterations[i] = -1;
		}
	}

	struct eckpunkt
	{
		uint x, y;
		eckpunkt(uint m_x, uint m_y)
			: x(m_x), y(m_y) { }
	};

	void updateCoordinates()
	{
		for(unsigned int wpos = 0; wpos < width; wpos++)
		{
			wCoordinates[wpos] = (center.real() - ( radius * (T(width) / height))
									  + ( 2 * radius * (T(width) / height)) * T(wpos) / width);
		}
		for(unsigned int hpos = 0; hpos < height; hpos++)
		{
			hCoordinates[hpos] = -( ( -center.imag() - radius ) + ( ( 2 * radius * hpos ) / height ) );
		}
	}

	int calculatePixel(uint wpos, uint hpos)
	{
		if(exponent == 2.0f)
		{
			T zr = wCoordinates[wpos];
			T zi = hCoordinates[hpos];
			T zrsqr = zr * zr;
			T zisqr = zi * zi;
			int current_iteration = 0;
			while(current_iteration < max_iterations && zrsqr + zisqr <= max_magnitude)
			{
				T tmp = zr + zi;
				zi = tmp * tmp - zrsqr - zisqr;
				zi += hCoordinates[hpos];
				zr = zrsqr - zisqr + wCoordinates[wpos];
				zrsqr = zr * zr;
				zisqr = zi * zi;
				current_iteration++;
			}
			return current_iteration;
		}
		else
		{
			std::complex<T> z(wCoordinates[wpos], hCoordinates[hpos]);
			std::complex<T> c = z;
			int current_iteration = 0;
			T previousMagnitude = z.real()*z.real() + z.imag()*z.imag();
			while(current_iteration < max_iterations && previousMagnitude < max_magnitude)
			{
				z = std::pow(z, exponent) + c;
				T newMagnitude = z.real()*z.real() + z.imag()*z.imag();
				if(newMagnitude == previousMagnitude)
				{
					current_iteration = max_iterations;
					break;
				}
				previousMagnitude = newMagnitude;
				current_iteration++;
			}
			return current_iteration;
		}
	}

	QColor getColor(int last_iteration)
	{
		if(last_iteration == max_iterations) // Number is in the set
		{
			return Qt::black;
		}
		else
		{
			switch(color_mode)
			{
				case Mb::NumberToColor:
				{
					uint8_t r = last_iteration * colorWeights[0] % 255;
					uint8_t g = last_iteration * colorWeights[1] % 255;
					uint8_t b = last_iteration * colorWeights[2] % 255;
					return QColor(r, g, b);
				}
				case Mb::ColorList:
				{
					Mb::rgb_color c = Mb::rgb_colors[last_iteration % 5 + 1];
					return QColor(c.r, c.g, c.b);
				}
			}
		}
		return Qt::black;
	}

	void colorPixel(uint wpos, uint hpos, int last_iteration)
	{
		if(last_iteration == max_iterations) // Number is in the set
		{
			image->setPixelColor(wpos, hpos, Qt::black);
		}
		else
		{
			image->setPixelColor(wpos, hpos, getColor(last_iteration));
		}
	}

	void rechteckBerechnen(eckpunkt e, uint fraction)
	{
		uint wpos = e.x;
		uint hpos = e.y;
		bool allEqual = true;
		int prev_iteration = calculatePixel(wpos, hpos);
		while(wpos < e.x + width/fraction)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			wpos++;
		}
		wpos--;
		while(hpos < e.y + height/fraction)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			hpos++;
		}
		hpos--;
		while(wpos > e.x)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			wpos--;
		}
		while(hpos > e.y)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			hpos--;
		}
	}

	void splitHorizontal(eckpunkt e, int fraction, bool top)
	{
//		if(height/fraction == 0)
		if(fraction == 4)
			return;
		int wpos = e.x;
		int hpos;
		if(height/fraction < height)
			hpos = e.y + height/fraction;
		else
			e.y + height/fraction - 0;

		do
		{
			int last_iteration = calculatePixel(wpos, hpos);
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			wpos++;
		} while(getMaxIterationAt(wpos, hpos) == -1);

		splitVertical(e, fraction, true);
		splitVertical({e.x, e.y+height/fraction}, fraction, false);
	}
	void splitVertical(eckpunkt e, int fraction, bool left)
	{
//		if(width/fraction == 0)
		if(fraction == 4)
			return;

//		bool allEqual = true;
		int wpos = e.x;
		int hpos = e.y;
		int prev_iteration = 0; //getMaxIterationAt(e);
//		if(left)
//		{
//			do
//			{
//				if(getMaxIterationAt(wpos, hpos) != prev_iteration)
//				{
//					allEqual = false;
//					break;
//				}
//				else
//				{
//					prev_iteration = getMaxIterationAt(wpos, hpos);
//				}
//				wpos++;
//			} while(getMaxIterationAt(wpos, hpos+1) == -1);
//			if(allEqual)
//			{
//				do
//				{
//					if(getMaxIterationAt(wpos, hpos) != prev_iteration)
//					{
//						allEqual = false;
//						break;
//					}
//					else
//					{
//						prev_iteration = getMaxIterationAt(wpos, hpos);
//					}
//					hpos++;
//				} while();
//			}
//			if(allEqual)
//			{
//				do
//				{

//				} while();
//			}
//		}
//		else
//		{
//			do
//			{

//			} while();
//			do
//			{

//			} while();
//			do
//			{

//			} while();
//		}


		if(width/(2*fraction) < width)
			wpos = e.x + width/(2*fraction);
		else
			wpos = e.x + width/(2*fraction)-0;
		hpos = e.y;

		do
		{
			int last_iteration = calculatePixel(wpos, hpos);
			colorPixel(wpos, hpos, last_iteration);
			setMaxIterationAt(wpos, hpos, last_iteration);
			hpos++;
		} while(getMaxIterationAt(wpos, hpos) == -1);

		splitHorizontal(e, fraction*2, false);
		splitHorizontal({e.x+width/(fraction*2), e.y}, fraction*2, true);
	}

public:

	QImage* compute()
	{
		Mb::Timer t;
		std::cout << "Creating Image " << "/ ";
		std::cout.flush();
		image->fill(Qt::white);
		clearArrMaxIterationAt();

		eckpunkt e1 = {0,0};
		eckpunkt e2 = {width/2, 0};
		eckpunkt e3 = {0, height/2};
		eckpunkt e4 = {width/2, height/2};

		QFuture<void> f1 = QtConcurrent::run(this, &MandelbrotSetHalf<T>::rechteckBerechnen, e1, 2);
		QFuture<void> f2 = QtConcurrent::run(this, &MandelbrotSetHalf<T>::rechteckBerechnen, e2, 2);
		QFuture<void> f3 = QtConcurrent::run(this, &MandelbrotSetHalf<T>::rechteckBerechnen, e3, 2);
		QFuture<void> f4 = QtConcurrent::run(this, &MandelbrotSetHalf<T>::rechteckBerechnen, e4, 2);

		f1.waitForFinished();
		f2.waitForFinished();
		f3.waitForFinished();
		f4.waitForFinished();

		splitVertical(e1, 2, true);
		splitVertical(e2, 2, true);
		splitVertical(e3, 2, false);
		splitVertical(e4, 2, false);

		return image;
	}

	// Setter
	void setCenter(std::complex<T> m_center)
	{
		center = m_center;
		updateCoordinates();
	}

	void setCenter(T m_re, T m_im)
	{
		center = {m_re, m_im};
		updateCoordinates();
	}

	void setCenter(int wpos, int hpos)
	{
		updateCoordinates();
		center = {wCoordinates[wpos], hCoordinates[hpos]};
		updateCoordinates();
	}

	void setSize(uint m_width, uint m_height)
	{
		if(m_width == width && m_height == height)
			return;
		width = m_width;
		height = m_height;
		wCoordinates.resize(width);
		hCoordinates.resize(height);

		if(image != nullptr)
		{
			delete image;
		}
		image = new QImage(width, height, QImage::Format_RGB32);

		if(arrMaxIterations != nullptr)
		{
			delete[] arrMaxIterations;
			arrMaxIterations = new int[width*height];
		}
		else
		{
			arrMaxIterations = new int[width*height];
		}
		clearArrMaxIterationAt();
		updateCoordinates();
	}

	MandelbrotSetHalf(unsigned int m_width, unsigned int m_height)
	{
		setSize(m_width, m_height);
	}

	void setRadius(T m_radius) { radius = m_radius; updateCoordinates(); }
	void setMaxIterations(int m_max_iterations) { max_iterations = m_max_iterations; }
	void setExponent(float m_exponent) { exponent = m_exponent; }
	void setMaxMagnitude(float m_max_magnitude) { max_magnitude = m_max_magnitude; }
	void setColorMode(Mb::ColorMode m_color_mode) { color_mode = m_color_mode; }
	void setColorWeights(int index, int weight) { colorWeights[index] = weight; }

	// Getter
	std::complex<T> getCenter() const { return center; }
	T getRadius() const { return radius; }
	int getMaxIterations() const { return max_iterations; }
	float getExponent() const { return exponent; }
	float getMaxMagnitude() const { return max_magnitude; }
	QImage* getQImagePtr() const { return image; }
};
