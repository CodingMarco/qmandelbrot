#pragma once

#include <complex>
#include <iostream>
#include <QImage>
#include <QPainter>
#include "mbnamespace.h"
#include <QLabel>
#include <QtConcurrent/QtConcurrent>
#include <QSize>
#include <QObject>
#include <QLine>
#include <QPainter>

class MandelbrotSet : public QObject
{
	Q_OBJECT
private:
	QImage* image = nullptr;
	QImage* sequenceImage = nullptr;
	bool sequence = false;
	Mb::ColorMode color_mode = Mb::NumberToColor;
	unsigned int width = 0, height = 0;
	std::complex<long double> center = {-2, 0};
	long double radius = 2.0;
	int max_iterations = 50;
	float exponent = 2.0f;
	float max_magnitude = 2.0f;
	std::vector<long double> hCoordinates, wCoordinates; // imaginary parts, real parts
	int colorWeights[3] = {8, 25, 4}; // R, G, B
	bool debug = false;
	QPainter* painter = nullptr;
	struct eckpunkt
	{
		uint x, y;
		eckpunkt(uint m_x, uint m_y)
			: x(m_x), y(m_y) { }
	};

	void updateCoordinates()
	{
		for(unsigned int wpos = 0; wpos < width; wpos++)
		{
			wCoordinates[wpos] = (center.real() - ( radius * ((long double)(width) / height))
									  + ( 2 * radius * ((long double)(width) / height)) * (long double)(wpos) / width);
		}
		for(unsigned int hpos = 0; hpos < height; hpos++)
		{
			hCoordinates[hpos] = -( ( -center.imag() - radius ) + ( ( 2 * radius * hpos ) / height ) );
		}
	}

	QVector<std::complex<long double>> getSequence(int wpos, int hpos, int max_iter)
	{
		QVector<std::complex<long double>> s;
		std::complex<long double> z(wCoordinates[wpos], hCoordinates[hpos]);
		std::complex<long double> c = z;
		int current_iteration = 0;
		long double magnitude = z.real()*z.real() + z.imag()*z.imag();
		while(current_iteration < max_iter && magnitude < 32.0f)
		{
			z = std::pow(z, exponent) + c;
			s.append(z);
			magnitude = z.real()*z.real() + z.imag()*z.imag();
			current_iteration++;
		}
		return s;
	}

	int calculatePixel(uint wpos, uint hpos)
	{
		if(exponent == 2.0f)
		{
			long double zr = wCoordinates[wpos];
			long double zi = hCoordinates[hpos];
			long double zrsqr = zr * zr;
			long double zisqr = zi * zi;
			int current_iteration = 0;
			while(current_iteration < max_iterations && zrsqr + zisqr <= 2*max_magnitude)
			{
				long double tmp = zr + zi;
				zi = tmp * tmp - zrsqr - zisqr;
				zi += hCoordinates[hpos];
				zr = zrsqr - zisqr + wCoordinates[wpos];
				zrsqr = zr * zr;
				zisqr = zi * zi;
				current_iteration++;
			}
			return current_iteration;
		}
		else
		{
			std::complex<long double> z(wCoordinates[wpos], hCoordinates[hpos]);
			std::complex<long double> c = z;
			int current_iteration = 0;
			long double previousMagnitude = z.real()*z.real() + z.imag()*z.imag();
			while(current_iteration < max_iterations && previousMagnitude < max_magnitude)
			{
				z = std::pow(z, exponent) + c;
				long double newMagnitude = z.real()*z.real() + z.imag()*z.imag();
				if(newMagnitude == previousMagnitude)
				{
					current_iteration = max_iterations;
					break;
				}
				previousMagnitude = newMagnitude;
				current_iteration++;
			}
			return current_iteration;
		}
	}

	QColor getColor(int last_iteration)
	{
		if(last_iteration == max_iterations) // Number is in the set
		{
			return Qt::black;
		}
		else
		{
			switch(color_mode)
			{
				case Mb::NumberToColor:
				{
					uint r = last_iteration * colorWeights[0] % 512;
					uint g = last_iteration * colorWeights[1] % 512;
					uint b = last_iteration * colorWeights[2] % 512;
					if(r > 255)
						r = 511 - r;
					if(g > 255)
						g = 511 - g;
					if(b > 255)
						b = 511 - b;

					return QColor(r, g, b);
				}
				case Mb::ColorList:
				{
					Mb::rgb_color c = Mb::rgb_colors[last_iteration % 5 + 1];
					return QColor(c.r, c.g, c.b);
				}
			}
		}
		return Qt::black;
	}

	void colorPixel(uint wpos, uint hpos, int last_iteration)
	{
		if(last_iteration == max_iterations) // Number is in the set
		{
			image->setPixelColor(wpos, hpos, Qt::black);
		}
		else
		{
			image->setPixelColor(wpos, hpos, getColor(last_iteration));
		}
	}

	void rechteckBerechnen(eckpunkt e, uint fraction)
	{
		if(width/fraction == 0 || height/fraction == 0)
		{
			return;
		}
		uint wpos = e.x;
		uint hpos = e.y;
		bool allEqual = true;
		int prev_iteration = calculatePixel(wpos, hpos);
		while(wpos < e.x + width/fraction)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			wpos++;
		}
		wpos--;
		while(hpos < e.y + height/fraction)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			hpos++;
		}
		hpos--;
		while(wpos > e.x)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			wpos--;
		}
		while(hpos > e.y)
		{
			int last_iteration = calculatePixel(wpos, hpos);
			if(last_iteration != prev_iteration)
				allEqual = false;
			colorPixel(wpos, hpos, last_iteration);
			hpos--;
		}

		if(!allEqual)
		{
			fraction *= 2;
			rechteckBerechnen(e, fraction);
			rechteckBerechnen({e.x+width/fraction,e.y}, fraction);
			rechteckBerechnen({e.x,e.y+height/fraction}, fraction);
			rechteckBerechnen({e.x+width/fraction,e.y+height/fraction}, fraction);
		}
		else if(!debug)
		{
			for(uint hpos = e.y; hpos < e.y+height/fraction; hpos++)
			{
				for(uint wpos = e.x; wpos < e.x+width/fraction; wpos++)
				{
					colorPixel(wpos, hpos, prev_iteration);
				}
			}
		}
		//if(fraction < 65)
			//emit(updateImage(image));
	}

public:
	MandelbrotSet(unsigned int m_width, unsigned int m_height)
	{
		setSize(m_width, m_height);
	}

	QImage* compute()
	{
		Mb::Timer t;
		std::cout << "Creating Image " << "/ ";
		std::cout.flush();

		image->fill(Qt::white);

		eckpunkt e1 = {0,0};
		eckpunkt e2 = {width/2, 0};
		eckpunkt e3 = {0, height/2};
		eckpunkt e4 = {width/2, height/2};

		QFuture<void> f1 = QtConcurrent::run(this, &MandelbrotSet::rechteckBerechnen, e1, 2);
		QFuture<void> f2 = QtConcurrent::run(this, &MandelbrotSet::rechteckBerechnen, e2, 2);
		QFuture<void> f3 = QtConcurrent::run(this, &MandelbrotSet::rechteckBerechnen, e3, 2);
		QFuture<void> f4 = QtConcurrent::run(this, &MandelbrotSet::rechteckBerechnen, e4, 2);

		f1.waitForFinished();
		f2.waitForFinished();
		f3.waitForFinished();
		f4.waitForFinished();

		return image;
	}

	int getNearestIndex(std::vector<long double> arr, long double n)
	{
		long double lastDiff = std::abs(arr[0]-n);
		for(int i = 0; i < arr.size(); i++)
		{
			long double newDiff = std::abs(arr[i]-n);
			if(newDiff > lastDiff)
				return i-1;
			else
				lastDiff = newDiff;
		}
		return arr.size()-1;
	}

	void drawCross(int wpos, int hpos)
	{
		int d = 4;
		painter->drawLine(wpos-d, hpos+d, wpos+d, hpos-d);
		painter->drawLine(wpos-d, hpos-d, wpos+d, hpos+d);
	}

	void drawSequence(int wpos, int hpos)
	{
		*sequenceImage = image->copy();
		sequence = true;
		drawCross(wpos, hpos);
		auto sequence = getSequence(wpos, hpos, 200);
		qDebug() << sequence.size();
		int lastw = wpos;
		int lasth = hpos;
		for(int i = 0; i < sequence.size(); i++)
		{
			int w = getNearestIndex(wCoordinates, sequence[i].real());
			int h = getNearestIndex(hCoordinates, sequence[i].imag());
			if(w > 0 && h > 0)
				drawCross(w,h);
			painter->drawLine(w, h, lastw, lasth);
			lastw = w;
			lasth = h;
		}
		emit updateImage(sequenceImage);
	}

	// Setter
	void setCenter(std::complex<long double> m_center)
	{
		center = m_center;
		updateCoordinates();
	}

	void setCenter(long double m_re, long double m_im)
	{
		center = {m_re, m_im};
		updateCoordinates();
	}

	void setCenter(int wpos, int hpos)
	{
		updateCoordinates();
		center = {wCoordinates[wpos], hCoordinates[hpos]};
		updateCoordinates();
	}

	void setSize(uint m_width, uint m_height)
	{
		width = m_width;
		height = m_height;
		wCoordinates.resize(width);
		hCoordinates.resize(height);
		if(image != nullptr)
			delete image;
		image = new QImage(width, height, QImage::Format_RGB32);
		if(sequenceImage != nullptr)
			delete sequenceImage;
		sequenceImage = new QImage(width, height, QImage::Format_RGB32);
		painter = new QPainter(sequenceImage);
		painter->setPen(Qt::white);
		updateCoordinates();
	}

	void setRadius(long double m_radius) { radius = m_radius; updateCoordinates(); }
	void setMaxIterations(int m_max_iterations) { max_iterations = m_max_iterations; }
	void setExponent(float m_exponent) { exponent = m_exponent; }
	void setMaxMagnitude(float m_max_magnitude) { max_magnitude = m_max_magnitude; }
	void setColorMode(Mb::ColorMode m_color_mode) { color_mode = m_color_mode; }
	void setColorWeights(int index, int weight) { colorWeights[index] = weight; }

	// Getter
	std::complex<long double> getCenter() const { return center; }
	long double getRadius() const { return radius; }
	int getMaxIterations() const { return max_iterations; }
	float getExponent() const { return exponent; }
	float getMaxMagnitude() const { return max_magnitude; }
	QSize getSize() { return {int(width), int(height)}; }
	QImage* getQImagePtr() const { return image; }
	int getColorWeights(int index) { return colorWeights[index]; }
	QString getColorWeightsStr()
	{
		return QString::number(colorWeights[0]) + ";"
			 + QString::number(colorWeights[1]) + ";"
			 + QString::number(colorWeights[2]);
	}
signals:
	void updateImage(QImage* image);
};
