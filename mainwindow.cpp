﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <cmath>
#include <QtConcurrent/QtConcurrent>
#include <unistd.h>
#include <QFileDialog>
#include <QMessageBox>
#include <QImage>
#include <QSize>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFile>
#include <sstream>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	setlocale(LC_NUMERIC, "de_DE.UTF-8");
	this->setLocale(QLocale::German);
	ms = new MandelbrotSet(ui->imageLabel->width(), ui->imageLabel->height());
	connect(ui->imageLabel, SIGNAL(Left_Mouse_Pressed(int, int)), this, SLOT(Left_Mouse_Pressed(int, int)));
	connect(ui->imageLabel, SIGNAL(Right_Mouse_Pressed(int, int)), this, SLOT(Right_Mouse_Pressed(int, int)));
	connect(ui->imageLabel, SIGNAL(Right_Mouse_Pressed_And_Moved(int, int)), this, SLOT(Right_Mouse_Pressed_And_Moved(int, int)));
	connect(ui->imageLabel, SIGNAL(Wheel_Turned(int)), this, SLOT(Wheel_Turned(int)));
	connect(ms, SIGNAL(updateImage(QImage*)), this, SLOT(updateImage(QImage*)));

	xmlFile.setFileName(xmlFileName);
	xmlFile.open(QIODevice::ReadWrite);
	xmlOut.setDevice(&xmlFile);
	xmlIn.setDevice(&xmlFile);
	xmlOut.setAutoFormatting(true);
	;

	{
//	QString dir = "/home/marco/_Mandelbrot-GFS_/exp-vid/";
//	uint length = 30;
//	uint fps = 25;
//	uint images = length * fps;
//	msf->setRadius(1.3);
//	msf->setCenter(std::complex<float>(0.0f, 0.0f));
//	msf->setSize(1280, 720);
//	msf->setMaxIterations(15);
//	msf->setColorMode(Mb::ColorList);

//	float ex_min = 0;
//	float ex_max = -4;

//	for(uint x = 0; x < images; x++)
//	{
//		float exponent = ((ex_max-ex_min)/images)*x + ex_min;
//		std::cout << exponent << ": " << x << " / " << images << std::endl;
//		msf->setExponent(exponent);
//		image = msf->compute();
//		QString path = dir + "mb_" + QString::number(x) + ".png";
//		image->save(path);
//	}

//	double grenze = 2.01;
//	double exponent;
//	for(uint x = 0; x < images-2*fps; x++)
//	{
//		double tmp = -expl(-0.006*x+log(grenze));
//		exponent = tmp+grenze;
//		std::cout << exponent << ": " << x << std::endl;
//		msf->setExponent(exponent);
//		image = msf->compute();
//		QString path = dir + "mb_" + QString::number(x) + ".png";
//		image->save(path);
//	}
//	for(uint x = 0; x < fps; x++)
//	{
//		double ex_lin = ((double(2)-exponent)/fps)*x+exponent;
//		std::cout << ex_lin << ": " << images-2*fps+x << std::endl;
//		msf->setExponent(ex_lin);
//		image = msf->compute();
//		QString path = dir + "mb_" + QString::number(images-2*fps+x) + ".png";
//		image->save(path);
//	}
//	msf->setExponent(2);
//	image = msf->compute();
//	for(uint x = 0; x < fps; x++)
//	{
//		std::cout << 2.0 << ": " << images-fps+x << std::endl;
//		QString path = dir + "mb_" + QString::number(images-fps+x) + ".png";
//		image->save(path);
//	}
}
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_pushButton_clicked()
{
	image = ms->compute();
	ui->imageLabel->setPixmap(QPixmap::fromImage(*image));
}

void MainWindow::on_comboBoxFarbmodus_currentIndexChanged(int index)
{
	switch(index)
	{
		case Mb::ColorList:
			ms->setColorMode(Mb::ColorList);
			break;
		case Mb::NumberToColor:
			ms->setColorMode(Mb::NumberToColor);
			break;
	}
}

void MainWindow::on_sliderExp_sliderMoved(int position)
{
	float exponent = float(position) / 10;
	ms->setExponent(exponent);
	ui->lblExp->setNum(double(exponent));
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::on_slderRange_sliderMoved(int range)
{
	long double radius = expl(-(long double)(ui->slderRange->maximum()-range) / 200 + log(2) );
	ms->setRadius(radius);
	ui->lblRadiusZahl->setNum(double(radius));
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::on_sliderMaxIter_sliderMoved(int maxIter)
{
	ui->spinBoxMaxIter->setValue(maxIter);
	ms->setMaxIterations(maxIter);
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::on_spinBoxMaxIter_valueChanged(int maxIter)
{
	ui->sliderMaxIter->setValue(maxIter);
	on_sliderMaxIter_sliderMoved(maxIter);
}

void MainWindow::on_sliderR_sliderMoved(int position)
{
	ui->lblRnum->setNum(position);
	ms->setColorWeights(0, position);
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::on_sliderG_sliderMoved(int position)
{
	ui->lblGnum->setNum(position);
	ms->setColorWeights(1, position);
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::on_sliderB_sliderMoved(int position)
{
	ui->lblBnum->setNum(position);
	ms->setColorWeights(2, position);
	if(ui->chkDyn->checkState())
		on_pushButton_clicked();
}

void MainWindow::Left_Mouse_Pressed(int wpos, int hpos)
{
	ms->setCenter(wpos, hpos);
	on_pushButton_clicked();
}

void MainWindow::Right_Mouse_Pressed(int wpos, int hpos)
{
	ms->drawSequence(wpos, hpos);
}

void MainWindow::Wheel_Turned(int degrees)
{
	if(ui->chkDyn->checkState())
	{
		long double radius = ms->getRadius() - ms->getRadius() * double(5) / degrees;
		ms->setRadius(radius);
		ui->lblRadiusZahl->setNum(double(radius));
		ui->slderRange->setValue(200*(logl(radius)-logl(2))+8000);
		on_pushButton_clicked();
	}
}

void MainWindow::Right_Mouse_Pressed_And_Moved(int wpos, int hpos)
{
	ms->drawSequence(wpos, hpos);
}

void MainWindow::on_cmdSetCenter_clicked()
{
	ms->setCenter({ui->doubleSpinBoxCenterReal->value(), ui->doubleSpinBoxCenterImag->value()});
	on_pushButton_clicked();
}

void MainWindow::on_cmdSave_clicked()
{
	if(image != nullptr)
	{
		QString filename = QFileDialog::getSaveFileName(this, tr("Save Image As"),
														QDir::homePath(), tr("Image Files (*.png *.jpg *.ppm *.jpeg *.bmp)"));
		ms->setSize(ui->spinBoxSaveWidth->value(), ui->spinBoxSaveHeight->value());
		image = ms->compute();
		image->save(filename);
		ms->setSize(ui->imageLabel->width(), ui->imageLabel->height());
	}
	else
	{
		// Open error dialog
		QMessageBox::critical(this, tr("Error"), tr("You must calculate the image before!"));
	}
}

void MainWindow::resizeEvent(QResizeEvent *ev)
{
	ms->setSize(ui->imageLabel->width(), ui->imageLabel->height());
}

void MainWindow::updateImage(QImage* image)
{
	static int i = 0;
	ui->imageLabel->setPixmap(QPixmap::fromImage(*image));
	ui->imageLabel->repaint();
	//image->save(QString("/home/marco/aaa/mb_") + QString::number(i) + ".png");
	i++;
}

void MainWindow::on_cmdSaveBookmark_clicked()
{
	// Center to QString
	std::stringstream s;
	s << ms->getCenter().real() << ";" << ms->getCenter().imag();
	QString centerStr = QString::fromStdString(s.str());

	// Radius to QString
	s.str(""); // clear strrinstream
	s << ms->getRadius() << ms->getRadius();
	QString radiusStr = QString::fromStdString(s.str());

	if(xmlFile.isOpen())
	{
		if(!xmlFile.exists())
		{
			xmlFile.close();
			xmlFile.open(QFile::ReadWrite);
		}
		xmlOut.writeStartDocument();
		xmlOut.writeStartElement("bookmarks");
		xmlOut.writeStartElement("bookmark");
		xmlOut.writeTextElement("id", 0);
		xmlOut.writeTextElement("name", "example Name");
		xmlOut.writeTextElement("colorWeights", ms->getColorWeightsStr());
		xmlOut.writeTextElement("center", centerStr);
		xmlOut.writeTextElement("radius", radiusStr);
		xmlOut.writeTextElement("colorMode", "colorList");
		xmlOut.writeTextElement("maxIterations", QString::number(ms->getMaxIterations()));
		xmlOut.writeTextElement("maxMagnitude", QString::number(ms->getMaxMagnitude()));
		xmlOut.writeEndElement();
		xmlOut.writeEndElement();
		xmlOut.writeEndDocument();
		xmlFile.flush();
	}
	else
	{
		QMessageBox::warning(this, "Warning", "File " + xmlFile.fileName() + "not open,\n"
																			 "bookmark won't be saved!");
	}
}
