#include "MandelbrotDisplay.h"
#include <iostream>

MandelbrotDisplay::MandelbrotDisplay(QWidget* parent) : QLabel(parent) { }

void MandelbrotDisplay::mousePressEvent(QMouseEvent *event)
{
	wpos = event->x();
	hpos = event->y();
	if(event->button() == Qt::LeftButton)
		emit Left_Mouse_Pressed(wpos, hpos);
	else if(event->button() == Qt::RightButton)
		rightMousePressed = true;
		emit Right_Mouse_Pressed(wpos, hpos);
	event->accept();
}

void MandelbrotDisplay::mouseReleaseEvent(QMouseEvent *event)
{
	if(event->button() == Qt::RightButton)
		rightMousePressed = false;
	event->accept();
}

void MandelbrotDisplay::mouseMoveEvent(QMouseEvent *event)
{
	wpos = event->x();
	hpos = event->y();
	if(rightMousePressed)
		emit Right_Mouse_Pressed_And_Moved(wpos, hpos);
	event->accept();
}

void MandelbrotDisplay::wheelEvent(QWheelEvent *event)
{
	int degrees = event->angleDelta().y() / 8;
	if(degrees != 0)
		emit Wheel_Turned(degrees);
	event->accept();
}
