#pragma once

#include <QLabel>
#include <QMouseEvent>
#include <QEvent>
#include <QWheelEvent>

class MandelbrotDisplay : public QLabel
{
	Q_OBJECT
public:
	explicit MandelbrotDisplay(QWidget* parent = 0);
private:
	int wpos, hpos;
	bool rightMousePressed = false;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent *event) override;
	void mouseMoveEvent(QMouseEvent *event) override;
	void wheelEvent(QWheelEvent* event) override;

signals:
	void Left_Mouse_Pressed(int wpos, int hpos);
	void Right_Mouse_Pressed(int wpos, int hpos);
	void Right_Mouse_Pressed_And_Moved(int wpos, int hpos);
	void Wheel_Turned(int degrees);
};
