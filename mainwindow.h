#pragma once

#include <QMainWindow>
#include "MandelbrotSetHalf.h"
#include "MandelbrotSet.h"
#include <QResizeEvent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_pushButton_clicked();
	void on_comboBoxFarbmodus_currentIndexChanged(int index);
	void on_slderRange_sliderMoved(int range);
	void on_sliderExp_sliderMoved(int position);
	void on_sliderMaxIter_sliderMoved(int maxIter);
	void on_spinBoxMaxIter_valueChanged(int maxIter);
	void on_sliderR_sliderMoved(int position);
	void on_sliderG_sliderMoved(int position);
	void on_sliderB_sliderMoved(int position);
	void Left_Mouse_Pressed(int wpos, int hpos);
	void Right_Mouse_Pressed(int wpos, int hpos);
	void Wheel_Turned(int degrees);
	void Right_Mouse_Pressed_And_Moved(int wpos, int hpos);
	void on_cmdSetCenter_clicked();
	void on_cmdSave_clicked();
	void on_cmdSaveBookmark_clicked();

private:
	Ui::MainWindow* ui;
	Mb::DataType datatype = Mb::Float;
	QImage* image = nullptr;
	MandelbrotSet* ms;
	void resizeEvent(QResizeEvent* ev);
	// Bookmarks XML
	QString xmlFileName = "/home/marco/amb/bookmarks.xml";
	QFile xmlFile;
	QXmlStreamReader xmlIn;
	QXmlStreamWriter xmlOut;

public slots:
	void updateImage(QImage* image);
};
