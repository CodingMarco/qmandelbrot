#pragma once

#include <stdint.h>
#include <chrono>
#include <iostream>

namespace Mb
{
	enum ColorMode
	{
		NumberToColor,
		ColorList
	};

	enum CountingMode
	{
		Random,
		Increment,
		Custom,
		Exponent,
		MaxMagnitude
	};

	enum DataType
	{
		Float,
		Double
	};

	struct rgb_color
	{
		uint8_t r, g, b;
		rgb_color(uint8_t m_r, uint8_t m_g, uint8_t m_b)
		{
			r = m_r;
			g = m_g;
			b = m_b;
		}
	};

	static rgb_color rgb_colors[] = {
										 {0  , 0  , 0  }, // black
										 {255, 58 , 58 }, // red
										 {102, 255, 102}, // green
										 {0  , 128, 255}, // blue
										 {255, 255, 0  }, // yellow
										 {255, 0  , 255}, // magenta
										 {0  , 255, 255}, // cyan
										 {255, 255, 255}  // white
									};

	struct Timer
	{
		std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
		std::chrono::duration<double> duration;

		Timer()
		{
			start = std::chrono::high_resolution_clock::now();
		}

		~Timer()
		{
			end = std::chrono::high_resolution_clock::now();
			duration = end - start;

			double ms = duration.count() * 1000.0;
			std::cout << "took " << ms << "ms" << std::endl;
		}
	};
}
